<?php
  use ideenfrische\ContaoGridBundle\ContentGrid;

  $GLOBALS['TL_CTE']['grid'] = array
  (
    'grid_start'      => ContentGrid::class,
    'grid_separator'  => ContentGrid::class,
    'grid_stop'       => ContentGrid::class,
  );

  $GLOBALS['TL_WRAPPERS']['start'][]      = 'grid_start';
  $GLOBALS['TL_WRAPPERS']['separator'][]  = 'grid_separator';
  $GLOBALS['TL_WRAPPERS']['stop'][]       = 'grid_stop';
?>
