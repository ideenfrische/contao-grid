<?php
  $GLOBALS['TL_LANG']['tl_content']['grid_legend'] = 'Grid';
  $GLOBALS['TL_LANG']['tl_content']['grid_margin'] = array('Abstände zwischen den Grid-Elementen');
  $GLOBALS['TL_LANG']['tl_content']['grid_margin_options'] = array(
    'flush'=> 'Keine Abständen',
    'small'=> 'Klein',
    'medium'=> 'Mittel',
    'large'=> 'Groß',
    'huge'=> 'Riesig'
  );
  $GLOBALS['TL_LANG']['tl_content']['grid_align'] = array('Ausrichtung');
  $GLOBALS['TL_LANG']['tl_content']['grid_seperator'] = array('Trennlinien einfügen');
  $GLOBALS['TL_LANG']['tl_content']['grid_reversed'] = array('Grid umkehren');

  $GLOBALS['TL_LANG']['tl_content']['grid_item_legend'] = 'Grid Item';
  $GLOBALS['TL_LANG']['tl_content']['grid_item_size'] = array('Größe');
  $GLOBALS['TL_LANG']['tl_content']['grid_item_text_align'] = array('Textausrichtung');
  $GLOBALS['TL_LANG']['tl_content']['grid_item_space_top'] = array('Abstände nach oben');
  $GLOBALS['TL_LANG']['tl_content']['grid_item_space_bottom'] = array('Abstände nach unten');
 ?>
