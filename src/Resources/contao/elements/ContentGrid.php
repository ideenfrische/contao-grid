<?php

namespace ideenfrische\ContaoGridBundle;

use \Contao\FrontendTemplate;

class ContentGrid extends \Haste\Frontend\AbstractFrontendModule
{

    /**
     * Check if template is not empty
     *
     * @return string
     */
    public function generate()
    {
        if ('BE' === TL_MODE) {
          return '';
        }

        return parent::generate();
    }


    protected function compile()
    {
        $this->Template = new FrontendTemplate('ce_'.$this->type);
        if($this->type === 'grid_start'){
          $this->Template->grid = $this->getGridClass();
        }
        if($this->type === 'grid_start' || $this->type === 'grid_separator'){
          $this->Template->grid_item = $this->getGridItemClass();


        }
    }
    private function getGridClass(){
      $class = 'grid';

      if($this->grid_margin){
        $class .= " grid--$this->grid_margin";
      }
      $grid_align = deserialize($this->grid_align);
      if($grid_align){
        foreach($grid_align as $value){
          $class .= " grid--$value";
        }
      }
      if($this->grid_seperator){
        $class .= ' grid--seperator';
      }
      if($this->grid_reversed){
        $class .= ' grid--reversed';
      }

      return $class;
    }

    private function getGridItemClass(){
      $class = 'grid__item';

      $grid_item_size = deserialize($this->grid_item_size);
      if($grid_item_size){
        foreach($grid_item_size as $value){
          $class .= " width-$value";
        }
      }
      $grid_item_text_align = deserialize($this->grid_item_text_align);
      if($grid_item_text_align){
        foreach($grid_item_text_align as $value){
          $class .= " text-$value";
        }
      }
      $grid_item_space_top = deserialize($this->grid_item_space_top);
      if($grid_item_space_top){
        foreach($grid_item_space_top as $value){
          if(strpos($value, 'inherit') !== false){
            $class .= "";
          }else{
            $newValue = str_replace("-default", "" , $value);
            $class .= " space-$newValue";
          }
        }
      }
      $grid_item_space_bottom = deserialize($this->grid_item_space_bottom);
      if($grid_item_space_bottom){
        foreach($grid_item_space_bottom as $value){
          if(strpos($value, 'inherit') !== false){
            $class .= "";
          }else{
            $newValue = str_replace("-default", "" , $value);
            $class .= " space-$newValue";
          }
        }
      }

      return $class;
    }
}

?>
