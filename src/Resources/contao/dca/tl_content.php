<?php

$GLOBALS['TL_DCA']['tl_content']['palettes']['grid_start'] = '{type_legend},type;{grid_legend},grid_margin,grid_align,grid_seperator,grid_reversed; {grid_item_legend},grid_item_size,grid_item_text_align,grid_item_space_top,grid_item_space_bottom;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID;{invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['grid_separator'] = '{type_legend},type;{grid_item_legend},grid_item_size,grid_item_text_align,grid_item_space_top,grid_item_space_bottom;{protected_legend:hide},protected;{expert_legend},guests,cssID;{invisible_legend},invisible,start,stop;';
$GLOBALS['TL_DCA']['tl_content']['palettes']['grid_stop'] = '{type_legend},type;{protected_legend:hide},protected;{expert_legend:hide},guests;{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['fields']['grid_margin'] = array(
  'inputType'               => 'select',
  'options'                 => array('flush','small','medium','large','huge'),
  'reference'               => &$GLOBALS['TL_LANG']['tl_content']['grid_margin_options'],
  'eval'                    => array('tl_class'=>'w50'),
  'sql'                     => "varchar(255) NOT NULL default 'medium'"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['grid_align'] = array(
  'inputType'               => 'select',
  'options_callback'        => array('tl_content_grid','getGridAlignOptions'),
  'eval'                    => array('tl_class'=>'w50','multiple'=>true,'chosen'=>true),
  'sql'                     => "text NULL"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['grid_seperator'] = array(
  'inputType'               => 'checkbox',
  'eval'                    => array('tl_class'=>'w50'),
  'sql'                     => "char(1) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['grid_reversed'] = array(
  'inputType'               => 'checkbox',
  'eval'                    => array('tl_class'=>'w50'),
  'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['grid_item_size'] = array(
  'inputType'               => 'select',
  'options_callback'        => array('tl_content_grid','getGridSizeOptions'),
  'eval'                    => array('tl_class'=>'w50','multiple'=>true,'chosen'=>true,'includeBlankOption'=>true),
  'sql'                     => "text NULL"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['grid_item_text_align'] = array(
  'inputType'               => 'select',
  'options_callback'        => array('tl_content_grid','getGridTextAlignOptions'),
  'eval'                    => array('tl_class'=>'w50','multiple'=>true,'chosen'=>true,'includeBlankOption'=>true),
  'sql'                     => "text NULL"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['grid_item_space_top'] = array(
  'inputType'               => 'select',
  'options_callback'        => array('tl_content_grid','getGridSpaceTopOptions'),
  'eval'                    => array('tl_class'=>'w50 clr','multiple'=>true,'chosen'=>true,'includeBlankOption'=>true),
  'sql'                     => "text NULL"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['grid_item_space_bottom'] = array(
  'inputType'               => 'select',
  'options_callback'        => array('tl_content_grid','getGridSpaceBottomOptions'),
  'eval'                    => array('tl_class'=>'w50','multiple'=>true,'chosen'=>true,'includeBlankOption'=>true),
  'sql'                     => "text NULL"
);

class tl_content_grid extends tl_content {
  private function getSizeOptions($o,$s=false){
    $sizes = [
      'default'=>'Mobil',
      'tab' => 'Tablet',
      'lap' => 'Laptop',
      'desk' => 'Desktop',
      'desk-wide' => 'Desktop Groß'
    ];

    $options = array();

    foreach($sizes as $key => $size){
      $opt = [];
      foreach($o as $aKey =>$align){
        $t = $align.'('.$size.')';
        if($s){
          if($key == 'default'){
            $opt['-'.$s.'-'.$aKey] = $t;
          }else{
            $opt[$key.'--'.$s.'-'.$aKey] = $t;
          }
        }else{
          if($key == 'default'){
            $opt['-'.$aKey] = $t;
          }else{
            $opt[$key.'--'.$aKey] = $t;
          }
        }
      }
      $options[$size] = $opt;
    }

    return $options;
  }


  public function getGridAlignOptions(){
    $aligns = [
      'start' => 'Links',
      'end' => 'Rechts',
      'center' => 'Mittig-Horizontal',
      'between' => 'Abstand dazwischen',
      'around' => 'Abstand Element',
      'top' => 'Oben',
      'middle' => 'Mittig-Vertikal',
      'bottom' => 'Unten',
      'stretch' => 'Automatisch'
    ];

    return $this->getSizeOptions($aligns);
  }

  public function getGridSizeOptions(){
    $size = [
      '1of1' => '100%',
      '7of8' => '87.5%',
      '4of5' => '80%',
      '3of4' => '75%',
      '2of3' => '66%',
      '5of8' => '62.5%',
      '3of5' => '60%',
      '1of2' => '50%',
      '2of5' => '40%',
      '3of8' => '37.5%',
      '1of3' => '33%',
      '1of4' => '25%',
      '1of5' => '20%',
      '1of6' => '16%',
      '1of8' => '12.5%',
      '1of12' => '8.3%',
      '0' => 'Versteckt'
    ];

    return $this->getSizeOptions($size);
  }

  public function getGridTextAlignOptions(){
    $textAlign = [
      'left'=>'Links',
      'center'=>'Zentriert',
      'right'=>'Rechts'
    ];

    return $this->getSizeOptions($textAlign);
  }

  public function getGridSpaceTopOptions(){
    $spaceTop = [
      'none'=>'Keiner',
      'quarter'=>'Viertel',
      'half'=>'Hälfte',
      'default'=>'Einfach',
      'double'=>'Doppelt',
      'triple'=>'Dreifach'
    ];

    return $this->getSizeOptions($spaceTop,'top');
  }
  public function getGridSpaceBottomOptions(){
    $spaceBottom = [
      'none'=>'Keiner',
      'quarter'=>'Viertel',
      'half'=>'Hälfte',
      'default'=>'Einfach',
      'double'=>'Doppelt',
      'triple'=>'Dreifach'
    ];

    return $this->getSizeOptions($spaceBottom,'bottom');
  }
}
